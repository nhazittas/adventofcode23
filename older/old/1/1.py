import sys

def fuel(mass):
    return mass//3-2

su=0
for line in sys.stdin:
     i = int(line)
     sf = 0
     f = fuel(i)
     while f > 0:
         sf+=f
         f = fuel(f)

     su+=sf
     print(sf,su)
