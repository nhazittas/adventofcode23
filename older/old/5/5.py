import sys
from collections import defaultdict

inputs = []
outputs = []

debug = False

def add(program, pc):
    if debug:
        print("ADD", program[pc:pc+4])
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = a + b
    if debug:
        print("\t [",c,"]","=", a+b,"=",a,"+",b)

    return pc + 4

def mult(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = a * b

    return pc + 4

def read(program, pc):

    ad = program[pc+1]
    i = inputs.pop()
    print("IN[",pc,"]",ad, "=", i)

    program[ad] = i

    return pc + 2

def write(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    ad = program[pc+1]
    o = ad if imm1 else program[ad]
    outputs.append(o)
    print("OUT[", pc,"]",ad,"=", o)

    return pc + 2

def jit(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]

    if a != 0:
        pc = b
    else:
        pc+=3

    return pc

def jif(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]

    if a == 0:
        pc = b
    else:
        pc+=3

    return pc

def lt(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = 1 if a < b else 0

    return pc + 4

def eq(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = 1 if a == b else 0

    return pc + 4

def halt(program, pc):
    print("HALT")
    print(program)
    exit(0)
    return pc + 1

def error(program, pc):
    print("You dun f up aaron")
    exit(1)

# 1 = add
# 2 = mult
# 3 = input
# 4 = output
# 5 = jump if true
# 6 = jump if false
# 7 = less than
# 8 = equals
# 99  = halt 
funMap = defaultdict(lambda : error, {1:add, 2:mult, 3:read, 4:write, 5:jit, 6:jif, 7:lt, 8:eq, 99:halt})


def run(program):
    pc = 0
    while True:
        todo = program[pc] % 100

        pc = funMap[todo](program, pc)

for line in sys.stdin:
     program = [int(l) for l in line.split(",")]

inputs.append(5)
run(program)
print(program[0])
