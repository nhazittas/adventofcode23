import sys
from collections import defaultdict

inputs = []
outputs = []

debug = False
#debug = True

def add(program, pc):
    if debug:
        print("ADD", program[pc:pc+4])
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = a + b
    if debug:
        print("\t [",c,"]","=", a+b,"=",a,"+",b)

    return pc + 4

def mult(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = a * b

    return pc + 4

def read(program, pc):
    if debug:
        print(inputs)
    ad = program[pc+1]
    i = inputs.pop()
    if debug:
        print("IN[",pc,"]",ad, "=", i)

    program[ad] = i

    return pc + 2

def write(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    ad = program[pc+1]
    o = ad if imm1 else program[ad]
    outputs.append(o)
    if debug:
        print("OUT[", pc,"]",ad,"=", o)

    return pc + 2

def jit(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]

    if a != 0:
        pc = b
    else:
        pc+=3

    return pc

def jif(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]

    if a == 0:
        pc = b
    else:
        pc+=3

    return pc

def lt(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = 1 if a < b else 0

    return pc + 4

def eq(program, pc):
    options = program[pc]//100
    
    imm1 = (options % 10) == 1
    options//=10

    imm2 = (options % 10) == 1

    a = program[pc+1]
    b = program[pc+2]
    c = program[pc+3]

    a = a if imm1 else program[a]
    b = b if imm2 else program[b]
    program[c] = 1 if a == b else 0

    return pc + 4

def halt(program, pc):
    if debug:
        print("HALT")
        print(program)
    # exit(0)
    return -1

def error(program, pc):
    print("You dun f up aaron")
    exit(1)

# 1 = add
# 2 = mult
# 3 = input
# 4 = output
# 5 = jump if true
# 6 = jump if false
# 7 = less than
# 8 = equals
# 99  = halt 
funMap = defaultdict(lambda : error, {1:add, 2:mult, 3:read, 4:write, 5:jit, 6:jif, 7:lt, 8:eq, 99:halt})


def run(program):
    pc = 0
    while pc != -1:
        todo = program[pc] % 100
        #print(pc, todo)
        pc = funMap[todo](program, pc)
    return outputs[-1]


def runWith(phaseSetting, inpt, program):
    inputs.clear()
    inputs.append(inpt)
    inputs.append(phaseSetting)
    #inputs = [inpt, phaseSetting]
    outputs.clear()

    return run(list(program))

for line in sys.stdin:
     program = [int(l) for l in line.split(",")]

nums = list(range(5))

import itertools

maxComb = -1
maxPower = -1

for comb in itertools.permutations(nums, 5):
    print(comb)
    nextInput = 0
    for c in comb:
        nextInput = runWith(c, nextInput, program)
    if nextInput > maxPower:
        maxPower = nextInput
        maxComb = c

print(maxComb, maxPower)

