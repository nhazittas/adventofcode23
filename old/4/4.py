import sys


numContained = 0
for line in sys.stdin:
    line = line.strip()
    if len(line) == 0:
        continue
    p1,p2 = line.split(",")
    p1 = [int(c) for c in p1.split("-")]
    p2 = [int(c) for c in p2.split("-")]
    print(" ",p1,p2)

    if p1[0] > p2[0]:
        p1,p2 = p2,p1
        print("SWAP")
    print(p1,p2)

